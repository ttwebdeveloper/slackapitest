<?php

namespace App\Helpers;

class SlackApiHelper
{
    public static function call($api_method, $slack_access_token='', $data=array())
    {
        $slack_api_base_url = 'https://slack.com/api/';
        $headers = array(
            'Content-Type: application/json'
        );
        if($slack_access_token)
        {
            $headers[] = 'Authorization: Bearer ' . $slack_access_token;
        }

        $ch = curl_init();

        curl_setopt( $ch,CURLOPT_URL, $slack_api_base_url.$api_method );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        if(!empty($data))
        {
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
        }
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}