<?php
/**
 * Created by PhpStorm.
 * User: aakash
 * Date: 10/04/17
 * Time: 6:26 PM
 */

namespace App\Helpers;

use App\Http\ApiResponse\Exceptions\ApiException;

class ApiHelper
{
    /**
     * @param string $string
     * @return ApiException
     */
    public static function getException(string $string) : ApiException
    {
        $exceptionData = config("apierrors.".$string);
        $exception = new ApiException($exceptionData);
        return $exception;
    }
    public static function getMinimumVersion(string $platform)
    {
        return config('apiguard.'.strtolower($platform).'MinVersion',0);
    }
    public static function getWarningVersion(string $platform)
    {
        return config('apiguard.'.strtolower($platform).'WarningVersion',0);
    }
    public static function getPlatform()
    {
        return ApiHelper::getHeaderValue(config('apiguard.platformKey'),0);
    }
    public static function getBuild()
    {
        return ApiHelper::getHeaderValue(config('apiguard.buildKey'),0);
    }
    public static function getHeaderValue(string $keyName)
    {
        $request = request();

        $key = $request->header($keyName);

        if (empty($key)) {
            // Try getting the key from elsewhere
            $key = $request->get($keyName);
        }

        return $key;
    }
}