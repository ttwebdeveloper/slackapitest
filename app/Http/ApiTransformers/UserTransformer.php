<?php
namespace App\Http\ApiTransformers;

use League\Fractal;
use App\User;
class UserTransformer extends Fractal\TransformerAbstract
{
    public function transform(User $user)
    {
        $data = array();
        $data['id'] = $user->id;
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        return $data;
    }
}
