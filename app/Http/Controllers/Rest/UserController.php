<?php

namespace App\Http\Controllers\Rest;

use App\Http\ApiTransformers\UserTransformer;
use App\Http\Controllers\BaseRestController;
use Illuminate\Http\Request;

class UserController extends BaseRestController
{
    public function show(Request $request)
    {
        return $this->respondWithItem($this->getUserDetails(), new UserTransformer());
    }
}
