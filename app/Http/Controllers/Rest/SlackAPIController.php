<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\BaseRestController;
use Illuminate\Http\Request;
use App\Helpers\SlackApiHelper;

class SlackAPIController extends BaseRestController
{
	private $slack_access_token;

	public function __construct()
    {
        parent::__construct();
        if(empty($this->slack_access_token))
        {
			$this->slack_access_token = 'xoxb-362344958278-jfbYi2mJEvx2zkLO1sP0xoyU';
		}
    } 

    public function checkAuthToken(Request $request)
    {
		return SlackApiHelper::call('auth.test',$this->slack_access_token);
    }

    public function getAllUsers(Request $request)
    {
		return SlackApiHelper::call('users.list',$this->slack_access_token);
    }

    public function getAllChannels(Request $request)
    {
		return SlackApiHelper::call('channels.list',$this->slack_access_token);
    }

    public function postMessage(Request $request)
    {
    	return SlackApiHelper::call('chat.postMessage', $this->slack_access_token, ['channel' =>$request->channel_id, 'text' => $request->message, 'username' => 'snapTeamTest App']);
    }
  
}
