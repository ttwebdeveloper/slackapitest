<?php

namespace App\Http\Controllers;

use App\Http\ApiResponse\Controllers\GuardController;
use Illuminate\Support\Facades\Auth;
use App\User;
use ApiHelper;

class BaseRestController extends GuardController
{

    private $userProfile = null;
    public function __construct()
    {
        parent::__construct();
       // $this->middleware('checkPlatform');
    }
    protected function getUserDetails() : User{
        if($this->userProfile==null)
        {
            $userProfile = Auth::guard()->user();
        }
        return $userProfile;
    }
    /**
     * Append any generic data to the 'meta' array.
     *
     * @param  Array $meta passed by reference.
     */
    protected function appendGenericMeta(& $meta)
    {
        //Generate the error meta for update warning if necessary
        $platform = ApiHelper::getPlatform();
        $build = ApiHelper::getBuild();
        $warningVersion = ApiHelper::getWarningVersion($platform);
        if(!empty($platform) && !empty($build) && $build<$warningVersion)
        {
            $meta['code'] = "GEN-WARNING-UPDATE";
            $meta['message']='Please update your app.';
        }
    }


}
