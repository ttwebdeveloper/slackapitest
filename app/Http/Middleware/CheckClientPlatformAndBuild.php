<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;

use ApiHelper;
class CheckClientPlatformAndBuild
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $platform = ApiHelper::getPlatform();
        $build = ApiHelper::getBuild();
        if(empty($platform) || empty($build))
        {
            throw ApiHelper::getException("noplatform");
        }
        $minBuild = ApiHelper::getMinimumVersion($platform);

        if($build<$minBuild)
        {
            throw ApiHelper::getException("outdatedplatform");
        }
        return $next($request);

    }
}
