<?php
/**
 * Created by PhpStorm.
 * User: aakash
 * Date: 10/04/17
 * Time: 7:49 PM
 */

namespace App\Http\ApiResponse\Exceptions;


use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class Handler
{
    public static function handleAuthenticationException(Request $request, AuthenticationException $exception)
    {
        $data = array();
        $data['code'] = $exception->getCode();
        $data['httpCode'] = 401;
        $data['cause'] = $exception->getMessage();
        $data['message'] = "Please Login or log out and log back in.";
        $data['status'] = "failure";
        $data['title'] = "Please Login";
        return response()->json($data, 401);
    }
    public static function handleException(Request $request, ApiException $exception)
    {
        $httpCode = $exception->getHttpCode();
        return response()->json($exception->asArray(),
            ($httpCode==0?400:$httpCode));
    }
}