<?php
/**
 * Created by PhpStorm.
 * User: aakash
 * Date: 10/04/17
 * Time: 6:07 PM
 */

namespace App\Http\ApiResponse\Exceptions;

use Exception;

class ApiException extends Exception
{
    private $httpCode;
    private $prettyMessage;
    private $title;
    /**
     * ApiException constructor.
     * @param string $errorData
     */
    public function __construct($errorData)
    {
        /* Sample array:

        ['cause' => "Make sure your build number is greater than the min supported build number.",
        'httpcode' => 400,
        'internalcode'=>1,
        'message' => "Please updated your app!",
        'title' => "Update Needed",]

        */
        parent::__construct($errorData['cause'], $errorData['internalcode']);
        $this->httpCode = $errorData['httpcode'];
        $this->prettyMessage = $errorData['message'];
        $this->title = $errorData['title'];
    }

    /**
     * @return mixed
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    /**
     * @param mixed $httpCode
     */
    public function setHttpCode($httpCode)
    {
        $this->httpCode = $httpCode;
    }

    /**
     * @return mixed
     */
    public function getPrettyMessage()
    {
        return $this->prettyMessage;
    }

    /**
     * @param mixed $prettyMessage
     */
    public function setPrettyMessage($prettyMessage)
    {
        $this->prettyMessage = $prettyMessage;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function asArray()
    {
        $data = array();
        $data['code'] = $this->getCode();
        $data['httpCode'] = $this->getHttpCode();
        $data['cause'] = $this->getMessage();
        $data['message'] = $this->getPrettyMessage();
        $data['status'] = "failure";
        $data['title'] = $this->getTitle();
        return $data;
    }
}
