<?php

namespace App\Http\ApiResponse\Controllers;

use Illuminate\Routing\Controller;
use App\Http\ApiResponse\Response\Response;
use App\Http\ApiResponse\Builders\ResponseBuilder;

abstract class GuardController extends Controller
{
    /**
     * @var Response
     */
    public $response;

    public function __construct()
    {
        // Add middleware

        // Check for user auth state.

        $this->response = ResponseBuilder::build();
    }

    /**
     * Respond with a single item and append any generic meta data needed
     * @param Data $data
     * @param callable|\League\Fractal\TransformerAbstract $transformer
     * @return Formatted response.
     */
    public function respondWithItem($data, $transformer)
    {
        $meta = array();
        $this->appendGenericMeta($meta);
        return $this->response->withItem($data, $transformer,null,$meta);
    }

    /**
     * Respond with a single item and append any generic meta data needed
     * @param Data $data
     * @param callable|\League\Fractal\TransformerAbstract $transformer
     * @return Formatted response.
     */
    public function respondWithCollection($data, $transformer)
    {
        $meta = array();
        $this->appendGenericMeta($meta);
        return $this->response->withCollection($data, $transformer, null, null, $meta);
    }
    /**
     * Append any generic data to the 'meta' array.
     *
     * @param  Array $meta passed by reference.
     */

    abstract protected function appendGenericMeta(& $meta);
}
