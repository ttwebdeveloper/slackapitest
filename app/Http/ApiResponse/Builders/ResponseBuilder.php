<?php

namespace App\Http\ApiResponse\Builders;

use App\Http\ApiResponse\Response\Response;
use League\Fractal\Manager;
use Request;

class ResponseBuilder
{

    /**
     * @param null $includes
     * @return Response
     */
    public static function build($includes = null)
    {
        // Let's instantiate the response class first
        $manager = new Manager;

        return new Response($manager);
    }

}
