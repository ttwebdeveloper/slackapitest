<?php

return [
    'outdatedplatform' => [
        'cause' => "Make sure your build number is greater than the min supported build number.",
        'httpcode' => 400,
        'internalcode'=>1,
        'message' => "Please updated your app!",
        'title' => "Update Needed",
    ],
    'noplatform' => [
        'cause' => "please pass the platform and build keys in the header.",
        'httpcode' => 400,
        'internalcode' => 2,
        'message' => "Please updated your app!",
        'title' => "Update Needed",
    ],
    'upcomingfeature' => [
        'cause' => "we've not yet built this feature.",
        'httpcode' => 400,
        'internalcode' => 0,
        'message' => "Ooops this feature is not yet ready!",
        'title' => "Impossible!",
    ],

];
