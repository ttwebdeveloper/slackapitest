<?php

return [
    'platformKey'     => 'Platform',
    'buildKey'        => 'Build',
    'android'         => 'android',
    'ios'              => 'ios',
    'androidMinVersion' => 15,
    'iosMinVersion' => 15,
    'androidWarningVersion' => 20,
    'iosWarningVersion' => 20,
];