<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('v1/user/register', 'Rest\AuthController@register');
Route::get('/v1/all_users','Rest\SlackAPIController@getAllUsers');
Route::get('/v1/all_channels','Rest\SlackAPIController@getAllChannels');
Route::get('v1/verify_access','Rest\SlackAPIController@checkAuthToken');
Route::post('v1/post_message','Rest\SlackAPIController@postMessage');